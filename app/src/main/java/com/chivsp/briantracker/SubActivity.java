package com.chivsp.briantracker;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.TableRow;
import android.widget.TableLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.graphics.Color;

public class SubActivity extends AppCompatActivity {

    private TextView nameView;
    private TextView HANDView;
    private TextView VPIPView;
    private TextView PFRView;
    private TextView threeBetView;
    private TextView fourBetView;
    private TextView CB_View;
    private ControlDB controlDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        controlDB = new ControlDB(getApplicationContext());

        PlayerData[] playerAll = controlDB.getAllPlayer();

        TableLayout tableLayout = findViewById(R.id.tableLayout);

        for (int i = 0; i < playerAll.length; i++) {

            TableRow tableRow = (TableRow)getLayoutInflater().inflate(R.layout.tablerow, null);

            nameView = tableRow.findViewById(R.id.name_view);
            HANDView = tableRow.findViewById(R.id.HAND_view);
            VPIPView = tableRow.findViewById(R.id.VPIP_view);
            PFRView = tableRow.findViewById(R.id.PFR_view);
            threeBetView = tableRow.findViewById(R.id.threeBet_view);
            fourBetView = tableRow.findViewById(R.id.fourBet_view);
            CB_View = tableRow.findViewById(R.id.CB_view);


            if((i+1)%2 == 1){
                nameView.setBackgroundColor(Color.DKGRAY);
                HANDView.setBackgroundColor(Color.DKGRAY);
                VPIPView.setBackgroundColor(Color.DKGRAY);
                PFRView.setBackgroundColor(Color.DKGRAY);
                threeBetView.setBackgroundColor(Color.DKGRAY);
                fourBetView.setBackgroundColor(Color.DKGRAY);
                CB_View.setBackgroundColor(Color.DKGRAY);
            }
            Player player = new Player(playerAll[i]);
            nameView.setText(player.getPlayerName());//プレイヤー名
            HANDView.setText(String.valueOf(player.getHandSu()));//HAND数
            VPIPView.setText(String.valueOf(player.getVpip()));//VPIP
            PFRView.setText(String.valueOf(player.getPreRaise()));//PFR
            threeBetView.setText(String.valueOf(player.getThreeBet()));//3Bet率
            fourBetView.setText(String.valueOf(player.getForBetPlus()));//4Bet+率
            CB_View.setText(String.valueOf(player.getCBet()));//CB率

            tableLayout.addView(tableRow, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        }

   }

    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_second, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }
    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        //アクション登録画面
        if (menuItem.getItemId() == R.id.action_register_action) {
            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動
            startActivity(intent);
        //新規登録
        }else {
            //unknownプレイヤー削除
            controlDB.deleteUnknown();

            //テーブルデータを削除
            for(int i = 0; i < Table.MAX_PAYER; i++)
            {
                Table.arrPlayer[i] = null;
                Table.arrStatus[i] = null;
            }

            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(SubActivity.this, MainActivity.class);
            // Activity起動 新規登録
            startActivity(intent);
        }
        return true;
    }

    @Override
    protected void onStop() {
        controlDB.close();
        super.onStop();
    }
}
