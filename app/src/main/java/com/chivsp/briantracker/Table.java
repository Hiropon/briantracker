package com.chivsp.briantracker;

public class Table {
    public static final int MAX_PAYER = 10;
    static Player arrPlayer[];
    static Status arrStatus[];

    static {
        arrPlayer = new Player[MAX_PAYER];
        arrStatus = new Status[MAX_PAYER];
    }

    /**
     * コンストラクタ
     * テーブルのコンストラクタ
     *
     */
    public Table(){

    }

    /**
     * プレイヤーを追加する。
     *
     * @param seatNum
     * @param playerName
     */
    public static void addPlayer(int seatNum,String playerName){
        arrPlayer[seatNum-1] = new Player(seatNum,playerName);
        arrStatus[seatNum-1] = Status.Fold;
    }

    /**
     * プレイヤーを削除する。
     *
     * @param seatNum
     */
    public static void delPlayer(int seatNum, ControlDB controlDB){

        String playerName = arrPlayer[seatNum-1].getPlayerName();
        if(playerName.equals("Unknown" + seatNum)){
            controlDB.delPlayer(playerName);
        }

        arrPlayer[seatNum-1] = null;
        arrStatus[seatNum-1] = null;

    }

    /**
     * プレイヤーを取得する。
     *
     * @param seatNum
     */
    public static Player getPlayer(int seatNum){

        return arrPlayer[seatNum-1];

    }

    /**
     * プレイヤのアクションを設定する。
     *
     * @param seatNum
     */
    public static void setStatus(int seatNum,Status status){

        arrStatus[seatNum-1] =  status;

    }

    /**
     * プレイヤのアクションを取得する。
     *
     * @param seatNum
     */
    public static String getStatus(int seatNum){

        return arrStatus[seatNum-1].value();

    }

    /**
     * Nextの処理を実行する。
     *
     */
    public static void doNext(ControlDB controlDB){

        for(int i=0;i < MAX_PAYER;i++){

            if(arrPlayer[i] == null)continue;

            arrPlayer[i].next(arrStatus[i]);
            controlDB.saveData(arrPlayer[i]);

        }

    }

    /**
     * CNextの処理を実行する。
     *
     */
    public static void doCNext(ControlDB controlDB){

        for(int i=0;i < MAX_PAYER;i++){

            if(arrPlayer[i] == null)continue;

            arrPlayer[i].cNext(arrStatus[i]);
            controlDB.saveData(arrPlayer[i]);

        }

    }
}
