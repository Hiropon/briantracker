package com.chivsp.briantracker;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.util.Log;



public class ControlDB extends CustomOpenHelper {
    private SQLiteDatabase db;

    public ControlDB(Context ctext) {
        super(ctext);
        db = super.getWritableDatabase();
    }

    public boolean hasPlayer(String name) {
        /*
          名前があるか
         */
        String sql = "SELECT * FROM PLAYER_INFO WHERE PLAYER_NAME = '" + name + "';";

        Cursor c = db.rawQuery(sql,null);
        int cnt = c.getCount();
        c.close();
        Log.d("debug", "hasPlayer(String name)");
        if(cnt == 0){
            return false;
        }
        return true;
    }


    public PlayerData getPlayerData(String name) {
        /*
          DB取得
         */
        String sql = "SELECT * FROM PLAYER_INFO WHERE PLAYER_NAME = '" + name + "';";

        Cursor c = db.rawQuery(sql,null);
        c.moveToFirst();
        /*
        player.setPlayerName(c.getString(1));
        player.setHandSu(c.getInt(2));
        player.setSankaSu(c.getInt(3));
        player.setRaiseSu(c.getInt(4));
        player.setThreeBetSu(c.getInt(5));
        player.setForBetPlusSu(c.getInt(6));
        player.setcBetSu(c.getInt(7));
        */
        PlayerData playerData = new PlayerData(c);
        c.close();
        Log.d("debug", "getPlayerData(String name)");
        return playerData;
    }

    public void deleteUnknown() {
        /*
          新規登録ボタン押下
         */
        db.delete("PLAYER_INFO", "PLAYER_NAME LIKE 'Unknown%'", null);
        Log.d("debug", "deleteUnknown()");
    }

    public void delPlayer(String name) {
        /*
          プレイヤー削除
         */
        db.delete("PLAYER_INFO", "PLAYER_NAME = ?", new String[]{name});
        Log.d("debug", "delPlayer(String name)");
    }

    public void saveData(Player player){
         /*
          DB登録
         */
        if(hasPlayer(player.getPlayerName())){
            //DBに名前があったら
            updateData(player);

        }else{
            //DBに名前がなかったら
            insertData(player);
        }
        Log.d("debug", "saveData(Player player)");
    }

    public void insertData(Player player){
         /*
          新規登録
         */
        ContentValues values = new ContentValues();
        values.put("PLAYER_NAME", player.getPlayerName());
        values.put("HAND_NUM", player.getHandSu());
        values.put("ACTION_NUM", player.getSankaSu());
        values.put("RAISE_NUM ", player.getRaisSu());
        values.put("THREE_BET_NUM", player.getThreeBetSu());
        values.put("FOUR_BET_NUM", player.getForBetPlusSu());
        values.put("CB_NUM", player.getcBetSu());

        db.insert("PLAYER_INFO", null, values);
        Log.d("debug", "insertData(Player player)");
    }

    public void updateData(Player player){
         /*
          更新
         */
        ContentValues values = new ContentValues();
        values.put("HAND_NUM", player.getHandSu());
        values.put("ACTION_NUM", player.getSankaSu());
        values.put("RAISE_NUM ", player.getRaisSu());
        values.put("THREE_BET_NUM", player.getThreeBetSu());
        values.put("FOUR_BET_NUM", player.getForBetPlusSu());
        values.put("CB_NUM", player.getcBetSu());

        db.update("PLAYER_INFO", values,"PLAYER_NAME = ?", new String[]{player.getPlayerName()});
        Log.d("debug", "updateData(Player player)");
    }

    public PlayerData[] getAllPlayer() {
        /*
          DBから全てのプレイヤーの統計情報取得
         */

        String sql = "SELECT * FROM PLAYER_INFO;";
        Cursor c = db.rawQuery(sql,null);
        int cnt = c.getCount();
        PlayerData[] playerAll = new PlayerData[cnt];

        int tablePlayerNum = 0;
        for (int i = 0; i < Table.MAX_PAYER; i++) {
            if(Table.arrPlayer[i] == null)continue;
            if(!hasPlayer(Table.arrPlayer[i].getPlayerName()))continue;
            playerAll[tablePlayerNum] = getPlayerData(Table.arrPlayer[i].getPlayerName());
            tablePlayerNum++;
        }

        c.moveToFirst();
        int k = tablePlayerNum;
        for (int i = 0; i < cnt; i++) {

            boolean actionPlayer = false;
            for (int j = 0; j < Table.MAX_PAYER; j++) {
                if(Table.arrPlayer[j] == null)continue;
                if(!hasPlayer(Table.arrPlayer[j].getPlayerName()))continue;
                if(Table.arrPlayer[j].getPlayerName().equals(c.getString(1))) actionPlayer = true;
            }
            if(!actionPlayer){
                playerAll[k] = new PlayerData(c);
                k++;
            }
            c.moveToNext();
        }
        c.close();
        Log.d("debug", "getAllPlayer()");
        return playerAll;
    }



}
