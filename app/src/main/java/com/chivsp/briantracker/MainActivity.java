package com.chivsp.briantracker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Typeface;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private BottomSheetDialog dialog;
    private ControlDB controlDB;
    private Button[] btnPlayer = new Button[Table.MAX_PAYER];
    private TextView[] tviewPlayer = new TextView[Table.MAX_PAYER];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controlDB = new ControlDB(getApplicationContext());

        Button nextButton =  findViewById(R.id.Next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //next
                Table.doNext(controlDB);
                initialize();
            }
        });

        Button cnextButton =  findViewById(R.id.CNext);
        cnextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cnext
                Table.doCNext(controlDB);
                initialize();

            }
        });

        tviewPlayer[0] = findViewById(R.id.StaticView1);
        btnPlayer[0] = findViewById(R.id.PlayerButton1);
        btnPlayer[0].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[0] != null) {
                    openDialog((Button) v, 1);
                }
            }
        });
        btnPlayer[0].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Toast.makeText(v.getContext(),"Player1(Long)が押されました",Toast.LENGTH_SHORT).show();
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[0] == null && Table.arrStatus[0] == null){
                    registerPlayerDialog(1, (Button)v, tviewPlayer[0]);
                }
                else{
                    Toast.makeText(v.getContext(),"非活性にします",Toast.LENGTH_SHORT).show();
                    notactive(1, btnPlayer[0], tviewPlayer[0]);
                }
                return true;
            }
        });

        tviewPlayer[1] = findViewById(R.id.StaticView2);
        btnPlayer[1] = findViewById(R.id.PlayerButton2);
        btnPlayer[1].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player2が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[1] != null) {
                    openDialog((Button) v, 2);
                }
            }
        });
        btnPlayer[1].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[1] == null){
                    registerPlayerDialog(2, (Button)v, tviewPlayer[1]);
                }
                else{
                    notactive(2, btnPlayer[1], tviewPlayer[1]);
                }
                return true;
            }
        });

        tviewPlayer[2] = findViewById(R.id.StaticView3);
        btnPlayer[2] = findViewById(R.id.PlayerButton3);
        btnPlayer[2].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[2] != null) {
                    openDialog((Button) v, 3);
                }
            }
        });
        btnPlayer[2].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[2] == null){
                    registerPlayerDialog(3, (Button)v, tviewPlayer[2]);
                }
                else{
                    notactive(3, btnPlayer[2], tviewPlayer[2]);
                }
                return true;
            }
        });

        tviewPlayer[3] = findViewById(R.id.StaticView4);
        btnPlayer[3] = findViewById(R.id.PlayerButton4);
        btnPlayer[3].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[3] != null) {
                    openDialog((Button) v, 4);
                }
            }
        });
        btnPlayer[3].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[3] == null){
                    registerPlayerDialog(4, (Button)v, tviewPlayer[3]);
                }
                else{
                    notactive(4, btnPlayer[3], tviewPlayer[3]);
                }
                return true;
            }
        });

        tviewPlayer[4] = findViewById(R.id.StaticView5);
        btnPlayer[4] = findViewById(R.id.PlayerButton5);
        btnPlayer[4].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[4] != null) {
                    openDialog((Button) v, 5);
                }
            }
        });
        btnPlayer[4].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[4] == null){
                    registerPlayerDialog(5, (Button)v, tviewPlayer[4]);
                }
                else{
                    notactive(5, btnPlayer[4], tviewPlayer[4]);
                }
                return true;
            }
        });

        tviewPlayer[5] = findViewById(R.id.StaticView6);
        btnPlayer[5] = findViewById(R.id.PlayerButton6);
        btnPlayer[5].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[5] != null) {
                    openDialog((Button) v, 6);
                }
            }
        });
        btnPlayer[5].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[5] == null){
                    registerPlayerDialog(6, (Button)v, tviewPlayer[5]);
                }
                else{
                    notactive(6, btnPlayer[5], tviewPlayer[5]);
                }
                return true;
            }
        });

        tviewPlayer[6] = findViewById(R.id.StaticView7);
        btnPlayer[6] = findViewById(R.id.PlayerButton7);
        btnPlayer[6].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[6] != null) {
                    openDialog((Button) v, 7);
                }
            }
        });
        btnPlayer[6].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[6] == null){
                    registerPlayerDialog(7, (Button)v, tviewPlayer[6]);
                }
                else{
                    notactive(7, btnPlayer[6], tviewPlayer[6]);
                }
                return true;
            }
        });

        tviewPlayer[7] = findViewById(R.id.StaticView8);
        btnPlayer[7] = findViewById(R.id.PlayerButton8);
        btnPlayer[7].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[7] != null) {
                    openDialog((Button) v, 8);
                }
            }
        });
        btnPlayer[7].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[7] == null){
                    registerPlayerDialog(8, (Button)v, tviewPlayer[7]);
                }
                else{
                    notactive(8, btnPlayer[7], tviewPlayer[7]);
                }
                return true;
            }
        });

        tviewPlayer[8] = findViewById(R.id.StaticView9);
        btnPlayer[8] = findViewById(R.id.PlayerButton9);
        btnPlayer[8].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[8] != null) {
                    openDialog((Button) v, 9);
                }
            }
        });
        btnPlayer[8].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[8] == null){
                    registerPlayerDialog(9, (Button)v, tviewPlayer[8]);
                }
                else{
                    notactive(9, btnPlayer[8], tviewPlayer[8]);
                }
                return true;
            }
        });

        tviewPlayer[9] = findViewById(R.id.StaticView10);
        btnPlayer[9] = findViewById(R.id.PlayerButton10);
        btnPlayer[9].setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Player1が押されました",Toast.LENGTH_SHORT).show();
                if(Table.arrPlayer[9] != null) {
                    openDialog((Button) v, 10);
                }
            }
        });
        btnPlayer[9].setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                // 配列の数値指定を後でenumとかに置き換えたい
                if(Table.arrPlayer[9] == null){
                    registerPlayerDialog(10, (Button)v, tviewPlayer[9]);
                }
                else{
                    notactive(10, btnPlayer[9], tviewPlayer[9]);
                }
                return true;
            }
        });

        // ステータスみてボタンとビューの表示更新処理
        for(int i = 0; i < btnPlayer.length; i++) {
            if(Table.arrPlayer[i] != null && Table.arrStatus[i] != null){
                displayButton(btnPlayer[i],Table.getStatus(i + 1));
                tviewPlayer[i].setText(Table.getPlayer(i + 1).getStatistics());
                displayStaticView(tviewPlayer[i], true);
            }
        }

    }
    // オプションメニューを作成する
    public boolean onCreateOptionsMenu(Menu menu){
        // menuにcustom_menuレイアウトを適用
        getMenuInflater().inflate(R.menu.custom_menu_main, menu);
        // オプションメニュー表示する場合はtrue
        return true;
    }

    // メニュー選択時の処理
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent intent;

        // 押されたメニューのIDで処理を振り分ける
        //プレイヤー情報一覧
        if (menuItem.getItemId() == R.id.action_disp_player) {// インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            intent = new Intent(MainActivity.this, SubActivity.class);
            // Activity起動
            startActivity(intent);
        //新規登録
        }else {
            //unknownプレイヤー削除
            controlDB.deleteUnknown();

            //表示系とテーブルデータを削除
            for(int i = 0; i < Table.MAX_PAYER; i++)
            {
                Table.arrPlayer[i] = null;
                Table.arrStatus[i] = null;
                btnPlayer[i].setBackgroundResource(R.drawable.circle_button_notactive);
                btnPlayer[i].setText("");
                displayStaticView(tviewPlayer[i], false);
            }

            // インテント作成  第二引数にはパッケージ名からの指定で、遷移先クラスを指定
            //intent = new Intent(MainActivity.this, MainActivity.class);
            // Activity起動
            //startActivity(intent);
        }

        return true;
    }

    /**
     * プレイヤー登録ダイアログの表示
     * @author Shingo Hiroki
     */
    private void registerPlayerDialog(final int tableNo, final Button btn, final TextView tview) {
        final EditText editText = new EditText(this);
        editText.setHint("プレイヤー名を入力");
        new AlertDialog.Builder(this)
                .setTitle("プレイヤー登録")
                .setMessage("プレイヤー名を登録して下さい。未入力の場合はunknownとなり、統計情報が記録されません。")
                .setView(editText)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 処理
                        Table.addPlayer(tableNo, editText.getText().toString());

                        //テーブルに同じ名前があったら、テーブルから統計情報を取得する。
                        String playerName = Table.getPlayer(tableNo).getPlayerName();
                        if(controlDB.hasPlayer(playerName)){
                            Table.arrPlayer[tableNo - 1] = new Player(controlDB.getPlayerData(playerName));
                        }
                        displayButton(btn,Table.getStatus(tableNo));

                        tview.setText(Table.getPlayer(tableNo).getStatistics());
                        displayStaticView(tview, true);
                    }
                })
                .show();
    }

    /**
     * アクション選択ダイアログの表示
     * @author Shingo Hiroki
     */
    private void openDialog(final Button btn, final int tableNo) {
        View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);

        ImageButton btnFold = (ImageButton) view.findViewById(R.id.btnFold);
        btnFold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Foldが押されました",Toast.LENGTH_SHORT).show();
                Table.setStatus(tableNo, Status.Fold);
                displayButton(btn,"F");
                dialog.dismiss();
            }
        });

        ImageButton btnCall = (ImageButton) view.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Callが押されました",Toast.LENGTH_SHORT).show();
                Table.setStatus(tableNo, Status.Call);
                displayButton(btn,"C");
                dialog.dismiss();
            }
        });

        ImageButton btnRaise = (ImageButton) view.findViewById(R.id.btnRaise);
        btnRaise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Raiseが押されました",Toast.LENGTH_SHORT).show();
                Table.setStatus(tableNo, Status.Call);
                displayButton(btn,"R");
                dialog.dismiss();
            }
        });

        ImageButton btnThree = (ImageButton) view.findViewById(R.id.btnThree);
        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Threeが押されました",Toast.LENGTH_SHORT).show();
                Table.setStatus(tableNo, Status.Three);
                displayButton(btn,"3");
                dialog.dismiss();
            }
        });

        ImageButton btnFour = (ImageButton) view.findViewById(R.id.btnFour);
        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),"Fourが押されました",Toast.LENGTH_SHORT).show();
                Table.setStatus(tableNo, Status.For);
                displayButton(btn,"4");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     * TextViewの表示・非表示にするメソッド
     * @author Shingo Hiroki
     * @param isVisible true=表示、false=非表示
     */
    private void displayStaticView(TextView tview, boolean isVisible){
        if(isVisible){
            tview.setVisibility(View.VISIBLE);
        } else{
            tview.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStop() {
        controlDB.close();
        super.onStop();
    }

    /**
     * 自画面遷移するメソッド
     * @author Hiro sakata
     */
    public void reload() {

        Intent intent = getIntent();
        //overridePendingTransition(0, 0);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    /**
     * 初期処理をするメソッド
     * @author Hiro sakata
     */
    public void initialize() {

        for(int i = 0; i < Table.MAX_PAYER; i++)
        {
            //活性状態になっている全てのボタンをアクションFとする。
            if(Table.arrPlayer[i] != null) {
                Table.setStatus(i+1, Status.Fold);
            }
        }
        //活性状態になっている各プレイヤーの統計情報をテーブルから取得し表示する。
        reload();

    }

    /**
     * 活性化buttonデザインを表示するメソッド
     * @author arisa ogawa
     */
    public void displayButton(Button button, String status) {

        if(status.equals("F")){
            button.setBackgroundResource(R.drawable.fbutton);
        }else if(status.equals("C")){
            button.setBackgroundResource(R.drawable.cbutton);
        }else if(status.equals("R")){
            button.setBackgroundResource(R.drawable.rbutton);
        }else if(status.equals("3")){
            button.setBackgroundResource(R.drawable.threebutton);
        }else if(status.equals("4")){
            button.setBackgroundResource(R.drawable.fourbutton);
        }

        button.setText(status);

    }

    /**
     * ボタンを非活性にするメソッド
     * @author arisa ogawa
     */
    public void notactive(final int tableNo, final Button btn, final TextView tview) {

        new AlertDialog.Builder(this)
                .setTitle("退席しますか？")
                //.setMessage("message")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Table.delPlayer(tableNo, controlDB);
                        btn.setBackgroundResource(R.drawable.circle_button_notactive);
                        btn.setText("");
                        displayStaticView(tview, false);
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();

    }
}
